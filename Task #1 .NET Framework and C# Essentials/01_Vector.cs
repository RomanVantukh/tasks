using System;

public class Vector
{
	// Coordianates.
	private double _x;
	private double _y;
	private double _z;

	// Default constructor.
	public Vector(): this(0, 0, 0)
	{
	}
	
	// Custom constructor.
	public Vector(double x, double y, double z)
	{
		_x = x;
		_y = y;
		_z = z;
	}
	
	// Get x cordinate.
	public double X
	{
		get
		{
			return _x;
		}
	}
	
	// Get y coordinate.
	public double Y
	{
		get
		{
			return _y;
		}
	}
	
	// Get z coordinate
	public double Z
	{
		get
		{
			return _z;
		}
	}
	
	// Get vector length.
	public double Length
	{
		get
		{
			return Math.Sqrt(ScalarProduct(this, this));
		}
	}
	
	// Comparison vectors.
	public override bool Equals(object obj)
    {
        Vector other = obj as Vector;

        if (other == null)
        {
            return false;
        }

        return ((_x == other._x) && (_y == other._y) && (_z == other._z));
    }

	// Compute hash code.
    public override int GetHashCode()
    {
        return (int)_x ^ (int)_y ^ (int)_z;
    }
	
	// String vector presentation.
	public override string ToString()
	{
		string vectorPresentation = String.Format("({0}; {1}; {2})", X, Y, Z);
		
		return vectorPresentation;
	}
	
	// Add two vectors.
	public static Vector Add(Vector firstTerm, Vector secondTerm)
	{
		double newX = firstTerm.X + secondTerm.X;
		double newY = firstTerm.Y + secondTerm.Y;
		double newZ = firstTerm.Z + secondTerm.Z;
		
		return new Vector(newX, newY, newZ);
	}
	
	// Substract vectors.
	public static Vector Subtract(Vector declining, Vector subtrahend)
	{
		Vector reverseToSubtrahend = new Vector(- subtrahend.X, - subtrahend.Y, - subtrahend.Z);
		
		return Vector.Add(declining, reverseToSubtrahend);
	}
	
	// Compute scalar product.
	public static double ScalarProduct(Vector firstMultiplier, Vector secondMultiplier)
	{
		double scalarProduct = firstMultiplier.X * secondMultiplier.X + firstMultiplier.Y * secondMultiplier.Y + firstMultiplier.Z * secondMultiplier.Z;
		
		return scalarProduct;
	}
	
	// Compute vector product.
	public static Vector VectorProduct(Vector firstMultiplier, Vector secondMultiplier)
	{
		double newX = firstMultiplier.Y * secondMultiplier.Z - firstMultiplier.Z * secondMultiplier.Y;
		double newY = firstMultiplier.Z * secondMultiplier.X - firstMultiplier.X * secondMultiplier.Z;
		double newZ = firstMultiplier.X * secondMultiplier.Y - firstMultiplier.Y * secondMultiplier.X;
		
		return new Vector(newX, newY, newZ);
	}
	
	// Compute triple pruduct.
	public static double TripleProduct(Vector firstMultiplier, Vector secondMultiplier, Vector thirdMultiplier)
	{
		return ScalarProduct(firstMultiplier, VectorProduct(secondMultiplier, thirdMultiplier));
	}
	
	// Compute angle between vectors.
	public static double AngleBetweenVectors(Vector firstVector, Vector secondVector)
	{
		double angleInCos = ScalarProduct(firstVector, secondVector) / (firstVector.Length * secondVector.Length);
		double angleInRadians = Math.Acos(angleInCos);
		double angleInGrad = 180.0 * angleInRadians / Math.PI;
		
		return angleInGrad;
	}
	
	// Compare vertors using ==.
	public static bool operator ==(Vector firstVector, Vector secondVector)
	{
		return Equals(firstVector, secondVector);
	}
	
	// Compare vectors using !=.
	public static bool operator !=(Vector firstVector, Vector secondVector)
	{
		return !Equals(firstVector, secondVector);
	}
	
	// Add vectors using operator +.
	public static Vector operator +(Vector firstTerm, Vector secondTerm)
	{
		return Add(firstTerm, secondTerm);
	}
	
	// Subtract vectors using operator -.
	public static Vector operator -(Vector declining, Vector subtrahend)
	{
		return Subtract(declining, subtrahend);
	}
}

public class EntryPoint
{
	// Entry point method.
	public static void Main()
	{
		// Initialisation.
		Vector firstVector = new Vector(1, 2, 3);
		Vector secondVector = new Vector(4, 5, 6);
		Vector thirdVector = new Vector(7, 8, 9);
		Vector fourthVector = new Vector(1, 2, 3);
		
		Console.WriteLine("Some operations with vectors:\n");
		
		// Adding and subtracting.
		Console.WriteLine("{0} + {1} = {2}", firstVector, secondVector, firstVector + secondVector);
		Console.WriteLine("{0} - {1} = {2}\n", secondVector, firstVector, Vector.Subtract(secondVector, firstVector));
		
		// Scalar product.
		Console.WriteLine("Scalar product:");
		Console.WriteLine("({0}, {1}) = {2}\n", firstVector, secondVector, Vector.ScalarProduct(firstVector, secondVector));
		
		// Vector product.
		Console.WriteLine("Vector product:");
		Console.WriteLine("[{0}, {1}] = {2}\n", firstVector, secondVector, Vector.VectorProduct(firstVector, secondVector));
		
		// Triple product.
		Console.WriteLine("Triple product:");
		Console.WriteLine("({0}, [{1}, {2}]) = {3}\n", firstVector, secondVector, thirdVector,
					Vector.TripleProduct(firstVector, secondVector, thirdVector));
		
		// Get length.
		Console.WriteLine("Length:");
		Console.WriteLine("|{0}| = {1}\n", firstVector, firstVector.Length);
		
		// Angle in grad.
		Console.WriteLine("Angle between vectors {0} and {1}", firstVector, secondVector);
		Console.WriteLine("{0} grad\n", Vector.AngleBetweenVectors(firstVector, secondVector));
		
		// Comparisons.
		Console.WriteLine("Are {0} and {1} equal? {2}\n", firstVector, secondVector, firstVector == secondVector);
		Console.WriteLine("Are {0} and {1} equal? {2}", firstVector, fourthVector, firstVector == fourthVector);
	}
}

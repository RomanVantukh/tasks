using System;

public class Rectangle
{	
	// Fields that represent rectangle size.
	private double _width;
	private double _height;
	
	// Default constructor.
	public Rectangle(): this(0, 0, 1, 1)
	{
	}
	
	// Custom constructor.
	public Rectangle(double x, double y, double width, double height)
	{
		X = x;
		Y = y;
		Width = width;
		Heigth = height;
	}
	
	// Get/set rectangle width.
	public double Width
	{
		get
		{
			return _width;
		}
		
		set
		{
			if(value > 0)
			{
				_width = value;
			}
			else
			{
				throw new ArgumentException("Width must be positive.");
			}
		}
	}
	
	// Get/set rectangle heigth.
	public double Heigth
	{
		get
		{
			return _height;
		}
		
		set
		{
			if(value > 0)
			{
				_height = value;
			}
			else
			{
				throw new ArgumentException("Height must be positive.");
			}
		}
	}
	
	// Get/set rectangle left x-coordinate.
	public double X
	{
		get;
		
		set;
	}
	
	// Get/set rectangle bottom y-coordinate.
	public double Y
	{
		get;
		
		set;
	}
	
	// Move rectangle on vector (differenceX; differenceY).
	public void Move(double differenceX, double differenceY)
	{
		X += differenceX;
		Y += differenceY;
	}
	
	// Compute rectangle string presentation.
	public override string ToString()
	{
		return String.Format("Left bottom point: ({0}; {1}) Size: {2} x {3}", X, Y, Width, Heigth);
	}
	
	// Compute minimum rectangle that contain other two rectangles.
	public static Rectangle UnionToOneRectangle(Rectangle firstRectangle, Rectangle secondRectangle)
	{
		double newX = Math.Min(firstRectangle.X, secondRectangle.X);
		double newY = Math.Min(firstRectangle.Y, secondRectangle.Y);
		double newWidth = Math.Max(firstRectangle.X + firstRectangle.Width, secondRectangle.X + secondRectangle.Width) - newX;
		double newHeigth = Math.Max(firstRectangle.Y + firstRectangle.Heigth, secondRectangle.Y + secondRectangle.Heigth) - newY;
		
		return new Rectangle(newX, newY, newWidth, newHeigth);
	}
	
	// Compute rectangle intersect.
	public static Rectangle Intersect(Rectangle firstRectangle, Rectangle secondRectangle)
	{
		double newX = Math.Max(firstRectangle.X, secondRectangle.X);
		double newY = Math.Max(firstRectangle.Y, secondRectangle.Y);
		double newWidth = Math.Min(firstRectangle.X + firstRectangle.Width, secondRectangle.X + secondRectangle.Width) - newX;
		double newHeigth = Math.Min(firstRectangle.Y + firstRectangle.Heigth, secondRectangle.Y + secondRectangle.Heigth) - newY;
		
		try
		{
			return new Rectangle(newX, newY, newWidth, newHeigth);
		}
		catch(ArgumentException)
		{
			return null;
		}
	}
}

public class EntryPoint
{
	// Entry point.
	public static void Main()
	{
		// Initialisation.
		Rectangle firstRectangle = new Rectangle(0, 0, 2, 2);
		Rectangle secondRectangle = new Rectangle(1, 3, 3, 3);
		
		// Print rectangles.
		Console.WriteLine("First rectangle:\n{0}\n", firstRectangle);
		Console.WriteLine("Second rectangle:\n{0}\n", secondRectangle);
		
		// Resizing first rectangle.
		firstRectangle.Width = 6;
		firstRectangle.Heigth = 3;
		
		Console.WriteLine("First rectangle after resizing:\n{0}\n", firstRectangle);
		
		// Moving second rectangle.
		secondRectangle.Move(1, -4);
		
		Console.WriteLine("Second rectangle after moving:\n{0}\n", secondRectangle);
		
		// Compute union and intersaction.
		Rectangle unionRectangle = Rectangle.UnionToOneRectangle(firstRectangle, secondRectangle);
		Rectangle intersectRectangle = Rectangle.Intersect(firstRectangle, secondRectangle);
		
		// Print results.
		Console.WriteLine("Union first and second rectangle:\n{0}\n", unionRectangle);
		Console.WriteLine("Intersection first and second rectangle:\n{0}\n", intersectRectangle);
	}
}
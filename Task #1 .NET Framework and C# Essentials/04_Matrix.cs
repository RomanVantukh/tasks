using System;
using System.Text;

public class Matrix
{
	// Array of matrix elements.
	private double[,] _elements;
	
	// Default constructor.
	public Matrix(): this(0, 0)
	{
	}
	
	// Custom constructor.
	public Matrix(int rowCount, int columnCount)
	{
		_elements = new double[rowCount, columnCount];
	}
	
	// Custom constructor.
	public Matrix(double[,] elements)
	{
		_elements = new double[elements.GetLength(0), elements.GetLength(1)];
		
		for(var i = 0; i < _elements.GetLength(0); i++)
		{
			for(var j = 0; j < _elements.GetLength(1); j++)
			{
				_elements[i, j] = elements[i, j];
			}
		}
	}
	
	// Get matrix row count.
	public int RowCount
	{
		get
		{
			return _elements.GetLength(0);
		}
	}
	
	// Get matrix column count.
	public int ColumnCount
	{
		get
		{
			return _elements.GetLength(1);
		}
	}
	
	// Get/set matrix element by index.
	public double this[int rowIndex, int columnIndex]
	{
		get
		{
			if(rowIndex >= 0 && rowIndex < RowCount && columnIndex >= 0 && columnIndex < ColumnCount)
			{
				return _elements[rowIndex, columnIndex];
			}
			else
			{
				throw new IndexOutOfRangeException("Index is out of range");
			}
		}
		
		set
		{
			if(rowIndex >= 0 && rowIndex < RowCount && columnIndex >= 0 && columnIndex < ColumnCount)
			{
				_elements[rowIndex, columnIndex] = value;
			}
			else
			{
				throw new IndexOutOfRangeException("Index is out of range");
			}
		}
	}
	
	// Compare matrixes.
	public override bool Equals(object obj)
	{
		Matrix other = obj as Matrix;
		
		if(other == null || other.RowCount != RowCount || other.ColumnCount != ColumnCount)
		{
			return false;
		}
		
		for(var i = 0; i < RowCount; i++)
		{
			for(var j = 0; j < ColumnCount; j++)
			{
				if(other[i, j] != this[i, j])
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	// Compute hash code.
	public override int GetHashCode()
	{
		int result = 0;
		
		for(var i = 0; i < RowCount; i++)
		{
			for(var j = 0; j < ColumnCount; j++)
			{
				result = result ^ (int) this[i, j];
			}
		}
		
		return result;
	}
	
	// Compute matrix string presentation.
	public override string ToString()
	{
		StringBuilder result = new StringBuilder();
		
		for(var i = 0; i < RowCount; i++)
		{
			for(var j = 0; j < ColumnCount; j++)
			{
				result.Append(String.Format("{0}\t", this[i, j]));
			}
			
			result.Append("\n");
		}
		
		return result.ToString();
	}
	
	// Add two same dimention matrixes.
	public static Matrix Add(Matrix firstTerm, Matrix secondTerm)
	{
		if(firstTerm.RowCount == secondTerm.RowCount && firstTerm.ColumnCount == secondTerm.ColumnCount)
		{
			Matrix result = new Matrix(firstTerm.RowCount, firstTerm.ColumnCount);
			
			for(var i = 0; i < result.RowCount; i++)
			{
				for(var j = 0; j < result.ColumnCount; j++)
				{
					result[i, j] = firstTerm[i, j] + secondTerm[i, j];
				}
			}
			
			return result;
		}
		else
		{
			throw new ArgumentOutOfRangeException("Dimentions are not equal.");
		}
	}
	
	// Subtract two same dimention matrixes.
	public static Matrix Subtract(Matrix declining, Matrix subtrahend)
	{
		if(declining.RowCount == subtrahend.RowCount && declining.ColumnCount == subtrahend.ColumnCount)
		{
			Matrix result = new Matrix(declining.RowCount, declining.ColumnCount);
			
			for(var i = 0; i < declining.RowCount; i++)
			{
				for(var j = 0; j < declining.ColumnCount; j++)
				{
					result[i, j] = declining[i, j] - subtrahend[i, j];
				}
			}
			
			return result;
		}
		else
		{
			throw new ArgumentOutOfRangeException("Dimetions are not equal.");
		}
	}
	
	// Multiply two matrixes.
	public static Matrix Multiply(Matrix firstMultiplier, Matrix secondMultiplier)
	{
		if(firstMultiplier.ColumnCount == secondMultiplier.RowCount)
		{
			Matrix result = new Matrix(firstMultiplier.RowCount, secondMultiplier.ColumnCount);
			
			for(var i = 0; i < result.RowCount; i++)
			{
				for(var j = 0; j < result.ColumnCount; j++)
				{
					double sum = 0;
					
					for(var k = 0; k < firstMultiplier.ColumnCount; k++)
					{
						sum += firstMultiplier[i, k] * secondMultiplier[k, j];
					}
					
					result[i, j] = sum;
				}
			}
			
			return result;
		}
		else
		{
			throw new ArgumentOutOfRangeException("First matrix column count is not equal to second matrix row count.");
		}
	}
	
	// Multiply matrix by scalar.
	public static Matrix Multiply(Matrix multiplier, double scalar)
	{
		Matrix result = new Matrix(multiplier.RowCount, multiplier.ColumnCount);
		
		for(var i = 0; i < result.RowCount; i++)
		{
			for(var j = 0; j < result.ColumnCount; j++)
			{
				result[i, j] = multiplier[i, j] * scalar;
			}
		}
		
		return result;
	}
	
	// Divide matrix by scalar.
	public static Matrix Divide(Matrix devedent, double scalar)
	{
		Matrix result = new Matrix(devedent.RowCount, devedent.ColumnCount);
		
		for(var i = 0; i < result.RowCount; i++)
		{
			for(var j = 0; j < result.ColumnCount; j++)
			{
				result[i, j] = devedent[i, j] / scalar;
			}
		}
		
		return result;
	}
	
	// Transpose matrix.
	public static Matrix Transpose(Matrix target)
	{
		Matrix result = new Matrix(target.ColumnCount, target.RowCount);
		
		for(var i = 0; i < result.RowCount; i++)
		{
			for(var j = 0; j < result.ColumnCount; j++)
			{
				result[i, j] = target[j, i];
			}
		}
		
		return result;
	}
	
	// Compute submatrix.
	public static Matrix GetSubMatrix(Matrix target, int rowIndex, int columnIndex, int newMatrixRowCount, int newMatrixColumnCount)
	{
		if(rowIndex >= 0 && newMatrixRowCount > 0 && columnIndex >= 0 && newMatrixColumnCount > 0 &&
						rowIndex + newMatrixRowCount <= target.RowCount && columnIndex + newMatrixColumnCount <= target.ColumnCount)
		{
			Matrix result = new Matrix(newMatrixRowCount, newMatrixColumnCount);
			
			for(var i = 0; i < result.RowCount; i++)
			{
				for(var j = 0; j < result.ColumnCount; j++)
				{
					result[i, j] = target[i + rowIndex, j + columnIndex];
				}
			}
			
			return result;
		}
		else
		{
			throw new IndexOutOfRangeException("Index is out of range.");
		}			
	}
	
	// Compare matrixes using ==.
	public static bool operator ==(Matrix firstMatrix, Matrix secondMatrix)
	{
		return Equals(firstMatrix, secondMatrix);
	}
	
	// Compare matrixes using !=.
	public static bool operator !=(Matrix firstMatrix, Matrix secondMatrix)
	{
		return !Equals(firstMatrix, secondMatrix);
	}
	
	// Add two matrixes using operator +.
	public static Matrix operator +(Matrix firstTerm, Matrix secondTerm)
	{
		return Add(firstTerm, secondTerm);
	}
	
	// Subtract two matrixes using operator -.
	public static Matrix operator -(Matrix declining, Matrix subtrahend)
	{
		return Subtract(declining, subtrahend);
	}
	
	// Multiply two matrixes using operator *.
	public static Matrix operator *(Matrix firstMultiplier, Matrix secondMultiplier)
	{
		return Multiply(firstMultiplier, secondMultiplier);
	}
	
	// Multiply matrix by scalar using operator *.
	public static Matrix operator *(Matrix multiplier, double scalar)
	{
		return Multiply(multiplier, scalar);
	}
	
	// Multiply matrix by scalar using operator *.
	public static Matrix operator *(double scalar, Matrix multiplier)
	{
		return Multiply(multiplier, scalar);
	}
	
	// Divide matrix by scalar using operator /.
	public static Matrix operator /(Matrix devedent, double scalar)
	{
		return Divide(devedent, scalar);
	}	
}

public class EntryPoint
{
	// Entry point.
	public static void Main()
	{
		// Initialisation.
		Matrix firstMatrix = new Matrix(new double[,]
			{ 
				{1, 3, 4},
				{0, 4, 5},
				{3, 10, 2},
				{3, 4, 8}
			});
		Matrix secondMatrix = new Matrix(4, 3);
		
		for(var i = 0; i < secondMatrix.RowCount; i++)
		{
			for(var j = 0; j < secondMatrix.ColumnCount; j++)
			{
				secondMatrix[i, j] = (2 + 3 * i) * (j + 1); 
			}
		}
		
		// Print metrixes.
		Console.WriteLine("First matrix:\n{0}", firstMatrix);
		Console.WriteLine("Second matrix:\n{0}", secondMatrix);
		
		// Adding adn subtract matrixes.
		Matrix addResult = Matrix.Add(firstMatrix, secondMatrix);
		Matrix subResult = firstMatrix - secondMatrix;
		
		// Print result of adding and subtracting.
		Console.WriteLine("Result addind first and second matrixes:\n{0}", addResult);
		Console.WriteLine("Result subtracting first and second matrixes:\n{0}", subResult);
		
		// Transpose first matrix.
		firstMatrix = Matrix.Transpose(firstMatrix);
		
		// Print first matrix.
		Console.WriteLine("First matrix after transposing:\n{0}", firstMatrix);
		
		// Multiply first matrix by second.
		Matrix multResult = firstMatrix * secondMatrix;
		
		// Print result of multiplying.
		Console.WriteLine("Result of multiplying first matrix by second:\n{0}", multResult);
		
		// Multiplying by scalar.
		Console.WriteLine("Result of multiplying second matrix by 10:\n{0}", Matrix.Multiply(secondMatrix, 10));
		
		// Get submatrix.
		Matrix subMatrix = Matrix.GetSubMatrix(firstMatrix, 1, 2, 2, 2);
		
		// Print submatrix.
		Console.WriteLine("Submatrix 2x2 from first matrix:\n{0}", subMatrix);
	}
}